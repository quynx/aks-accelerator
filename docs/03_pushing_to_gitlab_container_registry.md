# 03 - Manually Pushing a “Hello World” Image to Gitlab Container Registry
_Aim: To build and push a Container Image to Gitlab Container Registry._

## 3.0 Introduction
Before establishing our CI pipeline, we can check that we can push a container image to Gitlab Container Registry by hand.

Later (see [07 Building a Container Image with Gitlab CI/CD](07_gitlab_ci_build_image.md)), we will use Gitlab Runner to build and push our images automatically.

## 3.1 Authenticating with Gitlab Using a Personal Access Token
Rather than entering our Gitlab password on the command line, we can ask Gitlab to create a _Personal Access Token_.

### 3.1.1. Creating a Token
See [Personal Access Tokens](https://gitlab.com/profile/personal_access_tokens).

Give your token a name, grant it access to the Gitlab API, and click _Create personal access token_.

![gitlab personal_access_token](./img/03_gitlab_personal_access_token.png "Personal access token creation")


### 3.1.2. Docker Login
With our token in hand, we can login to Gitlab Container Registry using `docker login`.

Note that it is possible to pass a token on the command line, but not recommended.

```
➜  docker login registry.gitlab.com -u <username> -p <personal access token>
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
```

It is better to store the secret somewhere protected, and pass as follows:

```
➜  docker login registry.gitlab.com -u <username> --password-stdin < ~/.ssh/gitlab_personal_access_token
Login Succeeded
```

## 3.2 Building and Pushing an Image
Once authenticated, we can build our image and push it to Gitlab.

## 3.2.1 Building
This time, we use the same `-t` flag we used previously to add `registry.gitlab.com` as our repository.

> Note: If the image `tag` does not contain a fully qualified domain name (FQDN) then Docker will default to using the [Dockerhub registry](https://hub.docker.com) when the image is pushed.

```
➜  docker build -t registry.gitlab.com/citihub/aks-accelerator .
```

## 3.2.2 Pushing
```
➜  docker push registry.gitlab.com/citihub/aks-accelerator

The push refers to repository [registry.gitlab.com/citihub/aks-accelerator]
900024d99404: Pushed
ff84a7025df1: Pushed
a073c7233cd0: Pushed
344fb4b275b7: Pushed
bcf2f368fe23: Pushed
latest: digest: sha256:aa370361b132cb2975ce2ef065ba00adf936a3e09e5c65718dd892cb4a78ee1e size: 1370
```

> Note: if we want to push a specific image, we append `:tag` to our image, e.g.  `docker push registry.gitlab.com/citihub/aks-accelerator:myTag`. We will use this approach when we review Gitlab CI/CD in a later section. 

## 3.2.3 Viewing Our Image
We can then see our image in Gitlab successfully:

![gitlab container registry](./img/03_gitlab_container_pushed.png "Container successfully pushed")


## Next
In the next section, we will create a Kubernetes cluster on Azure.

< [02 Containerising a “Hello World” Application](02_containerising.md) | 03 Pushing a Container to Gitlab | [04 Creating an AKS Cluster](04_I_creating_aks_via_terraform.md) >