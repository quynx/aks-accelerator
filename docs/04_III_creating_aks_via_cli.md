# 04 III - Creating an AKS Cluster Using Azure CLI

## 4.0 Prerequisites
- Before getting started, ensure that you have Azure CLI up and running with the latest version.
- Next, install “kubectl” so we can talk to the Kubernetes Cluster later.
- Enter the following command in Azure CLI.

```
$ az aks install-cli
```

### 4.1 Create a Resource Group
Next, create a new Resource Group AKSPOC to deploy AKS into in Azure.

- The intent of creating a separate RG is to ensure we can clean up the deployment later if required.


```
$ az group create --name AKSPOC --location eastus
```

- The output below shows a successful creation of the new Resource Group

```
{
  "id": "/subscriptions/b4e90c03-f76e-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/AKSPOC",
  "location": "eastus",
  "managedBy": null,
  "name": "AKSPOC",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null,
  "type": null
}
```
### 4.2 Create a VNet
Next, create a VNET in Azure that will host the Kubernetes cluster, the Internal Load Balancer for the application(s) and the associated IP addresses



```
$ az network vnet create --name aksvnetpoc --resource-group akspoc --subnet-name default --address-prefix 10.10.10.0/24 --subnet-prefix 10.10.10.0/24
```
- The output below shows the configuration being created successfully
```
{
  "newVNet": {
    "addressSpace": {
      "addressPrefixes": [
        "10.10.10.0/24"
      ]
    },
    "ddosProtectionPlan": null,
    "dhcpOptions": {
      "dnsServers": []
    },
    "enableDdosProtection": false,
    "enableVmProtection": false,
    "etag": "W/\"c19ac370-3557-xxxx-xxxx-xxxxxxxxxxxx\"",
    "id": "/subscriptions/b4e90c03-f76e-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/akspoc/providers/Microsoft.Network/virtualNetworks/aksvnetpoc",
    "location": "eastus",
    "name": "aksvnetpoc",
    "provisioningState": "Succeeded",
    "resourceGroup": "akspoc",
    "resourceGuid": "54f5889c-5980-xxxx-xxxx-xxxxxxxxxxxx",
    "subnets": [
      {
        "addressPrefix": "10.10.10.0/24",
        "addressPrefixes": null,
        "delegations": [],
        "etag": "W/\"c19ac370-3557-xxxx-xxxx-xxxxxxxxxxxx\"",
        "id": "/subscriptions/b4e90c03-f76e-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/akspoc/providers/Microsoft.Network/virtualNetworks/aksvnetpoc/subnets/default",
        "interfaceEndpoints": null,
        "ipConfigurationProfiles": null,
        "ipConfigurations": null,
        "name": "default",
        "networkSecurityGroup": null,
        "provisioningState": "Succeeded",
        "purpose": null,
        "resourceGroup": "akspoc",
        "resourceNavigationLinks": null,
        "routeTable": null,
        "serviceAssociationLinks": null,
        "serviceEndpointPolicies": null,
        "serviceEndpoints": null,
        "type": "Microsoft.Network/virtualNetworks/subnets"
      }
    ],
    "tags": {},
    "type": "Microsoft.Network/virtualNetworks",
    "virtualNetworkPeerings": []
  }
}
```

### 4.3 Create an AKS cluster

- Next, start deploying the Kubernetes cluster into the VNET created
- Specify the “vnet-subnet-id”, which can be copied from the previous command output

> Note: This operation will take a few minutes to complete.
> Note: Specify the internal IP’s of the cluster using pod-cidr, service-cidr, docker-bridge-address command line parameters

```
$ az aks create --name akspoccluster \
--resource-group akspoc \
--location eastus \
--node-count 1 \
--vnet-subnet-id "/subscriptions/b4e90c03-f76e-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/akspoc/providers/Microsoft.Network/virtualNetworks/aksvnetpoc/subnets/default" \
--dns-name-prefix aks-test \
--pod-cidr A CIDR notation IP range from which to assign pod IPs when kubenet is used \
```

To use Advanced Networking, specify the following instead of `pod-cidr`:

--service-cidr A CIDR notation IP range from which to assign service cluster IPs \
--docker-bridge-address An IP address and netmask assigned to the Docker bridge

### 4.4 Service Principals

Deploying an Internal Application with Private IP Addresses

With the AKS cluster running, you can start deploying the sample application on it. To do that, we need to obtain the credentials for the cluster:

```
$ az aks get-credentials --resource-group akspoc --name akspoccluster
```
- You should see an output similar to the one below
```
Merged "akspoccluster" as current context in $HOME/.kube/config
```

During the cluster deployment process, the Azure platform will create a Service Principal (SPN) for the Kubernetes Cluster.

This SPN typically has the permissions to access and manage the AKS cluster. It operates in the background when you use the `kubectl` command.

In most cases, your Developers or IT Administrators will create an SPN for you in advance to use for the AKS Cluster.

For example, when you check the nodes of the AKS cluster:

```
$ kubectl get nodes

NAME                       STATUS   ROLES   AGE   VERSION
aks-nodepool1-36184632-0   Ready    agent   5m    v1.11.8
```

Retrieve the information about the SPN by checking the configuration file:

```
$ cat ~/.azure/aksServicePrincipal.json

{"b4e90c03-f76e-xxxx-xxxx-xxxxxxxxxxxx": {"client_secret": "05be92c2xxxxxxxxxxxx-", "service_principal": " 916baaa7-xxxx-xxxx-xxxx-xxxxxxxxxxxx"}}
```

Next, check if the SPN has been properly added to the AKS subnet by navigating from the GUI, and if not, add using the following steps:

- Navigate to Resource groups in the Azure Portal
- Select the AKSPOC resource group
- Select the aksvnet VNet
- Select Subnets and choose akscluster subnet
- Click Manage Users and click the + Add button
- In the Add dialog, select the Owner role (Contributor isn’t enough) and paste the Service Principal ID that was retrieved before
(e.g., 916baaa7-0241-xxxx-xxxx-xxxxxxxxxx)
- Hit SAVE.
- Note: It has been observed that sometimes if a lot oa applications or AKS clusters are being deployed concurrently, the SPN created might not be 
- correctly assigned to the Resource Group with appropriate permissions, hence these steps to ensure that the SPN is pcorrectly assigned.


#### 4.5 Deploying an Application to the Cluster
Next, create a YAML deployment file for the test application.
A sample YAML file is provided here to enable faster deployment testing.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: azure-vote-back
spec:
  replicas: 1
  selector:
    matchLabels:
      app: azure-vote-back
  template:
    metadata:
      labels:
        app: azure-vote-back
    spec:
      containers:
      - name: azure-vote-back
        image: redis
        resources:
          requests:
            cpu: 100m
            memory: 128Mi
          limits:
            cpu: 250m
            memory: 256Mi
        ports:
        - containerPort: 6379
          name: redis
---
apiVersion: v1
kind: Service
metadata:
  name: azure-vote-back
spec:
  ports:
  - port: 6379
  selector:
    app: azure-vote-back
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: azure-vote-front
spec:
  replicas: 1
  selector:
    matchLabels:
      app: azure-vote-front
  template:
    metadata:
      labels:
        app: azure-vote-front
    spec:
      containers:
      - name: azure-vote-front
        image: microsoft/azure-vote-front:v1
        resources:
          requests:
            cpu: 100m
            memory: 128Mi
          limits:
            cpu: 250m
            memory: 256Mi
        ports:
        - containerPort: 80
        env:
        - name: REDIS
          value: "azure-vote-back"
---
apiVersion: v1
kind: Service
metadata:
  name: azure-vote-front
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-internal: "true"
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: azure-vote-front
```

Open the file in Notepad in case you wish to edit variables for the app itself.

Towards the end of the file, you will see the following syntax:

```
apiVersion: v1
kind: Service
metadata:
  name: azure-vote-front
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-internal: "true"
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: azure-vote-front
````

> Note here that the ‘annotations’ line is required to setup an Internal Load Balancer to make sure the application is accessible only on your private internal network.