# 04 I - Creating an AKS Cluster Using Terraform
_Aim: to set up Azure Kubernetes Service using either Terraform or the Azure CLI._

 > Note: to follow along with this section, you will need to have Terraform and/or the Azure CLI installed. See [Prerequisites](00_prerequisites.md).
 
 > If you do not have local access to Terraform, be aware that Azure Cloud Shell has it preinstalled.

## 4.0 Introduction
In order to create an Azure Kubernetes Service (AKS) cluster, we need to create or reference several Azure objects:

### 4.0.1 Core Azure Requirements

| Resource Type | Purpose | 
| --- | --- | 
| **Cluster Resource Group** | To contain all of the resources we need for our cluster (for simpler management) |
| **Log Analytics Workspace** | To enable us to retrieve and analyse logs emitted by the cluster |
| **Kubernetes Service** | The managed Kubernetes cluster itself |
| **Network Resource Group** | To contain all of the resources we need for our cluster's network (for simpler management) |
| **Cluster Vnet** | The Virtual Network into which we will deploy our cluster's resources | 
| **Cluster Subnet** | The Subnet (within the above Virtual Network) into which we will deploy our cluster's resources | 

> Note: for additional information on Kubernetes networking, refer to [Appendix I - Kubernetes Networking](appendix_I_kubernetes_networking.md).


## 4.1 Background Information

### 4.1.1 A Note on Terraform
To manage the complexity and inter-dependencies of these objects, [Hashicorp Terraform](https://www.hashicorp.com/products/terraform/) is a useful tool.

It allows us to declare the infrastructure we want (the "desired state"), provision it, and then iterate upon it or destroy it, as required, in an idempotent manner. 

Achieving the same goals with Azure Resource Manager (ARM) templates, Azure CLI or indeed alternatives such as [Pulumi](http://pulumi.io) is also possible. 

> Note: the documentation in this repository is not intended to be a complete guide to Terraform, but the Terraform Configurations are commented, where necessary.


### 4.1.2 Terraform Commands
Using Terraform is typically a three stage process:

> Note: no Terraform "server" is required to use Terraform - only the `terraform` executable.

| Stage  | Command | Purpose | 
| --- | --- | --- |
| **Init** | `terraform init` | Needs running once, or when new modules or providers are required | 
| **Plan** | `terraform plan` | Use to 'dry run' changes to infrastructure |
| **Apply** | `terraform apply` | Use to implement changes to infrastructure |


To run the configurations in this repository, simply

- `az login`
- `az account set --subscription <a subscription id>`
- `cd terraform`

Then run Terraform Apply:

> Note: if you want to use an existing Vnet:
> - specify `false` when asked if you want to `create_new_network`
> - specify `""` (empty string) when asked for an `existing_subnet_id` 
> See the next section for further details

```

➜  terraform apply

var.create_new_network
  Enter a value: false
  
var.existing_subnet_id
  Enter a value: ""

var.location
  Enter a value: westeurope

var.project_name
  Enter a value: aks-accelerator

var.service_principal
  Enter a value: <a service principal application id>

var.service_principal_password
  Enter a value: <the corresponding service principal password>

```

> Note: if required, there are ways of storing these values as defaults. For simplicity here, we just allow the command line to prompt us.


### 4.1.3 Networks
The Terraform configurations in this repository can either be used to:

- Create a new Virtual Network and Subnet
- Use an existing Virtual Network and Subnet

If you wish to use an existing network, make sure to supply the Azure Resource ID of the existing network when prompted:

```
var.existing_subnet_id
  Enter a value: "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/xxxxxx/providers/Microsoft.Network/virtualNetworks/xxx"
```

You can find the Resource ID using the Azure CLI:

> Remember to pass in the specific `vnet-name`, `resource-group` and `name` from your own environment.

```
➜  az network vnet subnet show \
--vnet-name aks-network-rg-vnet \
--resource-group aks-network-rg \ 
--name aks-network-rg-subnet

{
  "addressPrefix": "10.0.0.0/16",
  "addressPrefixes": null,
  "delegations": [],
  "etag": "W/\"8c09ca91-e383-4c51-a3b0-d83d708de707\"",
  "id": "/subscriptions/b34d826c-465a-45f5-939d-32c665408d0a/resourceGroups/aks-network-rg/providers/Microsoft.Network/virtualNetworks/aks-network-rg-vnet/subnets/aks-network-rg-subnet",
  "interfaceEndpoints": null,
  "ipConfigurationProfiles": null,
  "ipConfigurations": null,
  "name": "aks-network-rg-subnet",
  "networkSecurityGroup": null,
  "provisioningState": "Succeeded",
  "purpose": null,
  "resourceGroup": "aks-network-rg",
  "resourceNavigationLinks": null,
  "routeTable": null,
  "serviceAssociationLinks": null,
  "serviceEndpointPolicies": null,
  "serviceEndpoints": null,
  "type": "Microsoft.Network/virtualNetworks/subnets"
}
```

The field you need is the `id`.


> Note: for additional information on Kubernetes networking, refer to [Appendix I - Kubernetes Networking](appendix_I_kubernetes_networking.md).


## Futher Information

- [04 II - Creating a Cluster with the Azure CLI](04_II_creating_aks_via_azure_portal.md)
- [04 III - Creating a Cluster with the Azure CLI](04_III_creating_aks_via_cli.md)

## Next
< [03 Containerising a “Hello World” Application](03_pushing_to_gitlab_container_registry.md) | 04 Creating an AKS Cluster | [05 Connecting to the AKS Cluster](05_connecting_to_aks.md) >